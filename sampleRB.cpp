#include <stdio.h>

enum color_t{
  RED, BLACK
};

class RBTree{
  private:
    struct TreeNode {
      int key;
      color_t color;
      TreeNode* left;
      TreeNode* right;
      TreeNode* parent;
    };

    static TreeNode* const nil;

    TreeNode* root = nullptr;

  public:
    /* Fill in with methods */
    RBTree(){
      root = nil;
    }

    void insert(int k){
      TreeNode* newNode = new TreeNode;
      newNode->key = k;

      if (root == nil){
        printf("Inserting %d into empty tree\n", k);
        root = newNode;
        newNode->parent = nil;
      } else {
        TreeNode* y = nil;
        TreeNode* x = root;
        while(x != nil){
          y = x;
          if( k < x->key ){
            x = x->left;
          } else {
            x = x->right;
          }
        }
        if( k < y->key ){
          printf("Inserting %d on the left of %d\n",k,y->key);
          y->left = newNode;
        } else {
          printf("Inserting %d on the right of %d\n",k,y->key);
          y->right = newNode;
        }
        newNode->parent = y;
      }
      newNode->color = RED;
      newNode->left = nil;
      newNode->right = nil;
      // Call fixup
    }
};

RBTree::TreeNode* const RBTree::nil = new TreeNode({0, BLACK, nullptr, nullptr, nullptr});

int main(){
  RBTree t;
  t.insert(10);
  t.insert(15);
  t.insert(1);
  return 0;
}

#include <iostream>
#include "rbTree.h"
#include <ctime>

using namespace std;

int main ()
{
    clock_t beg;
    clock_t end;
    rbTree r;
    for(int i=0; i < 19; i++)
    {
        beg = clock();
        r.insert(i);
        end = clock() - beg;
        cout << "Time: " << (double)end/((double)CLOCKS_PER_SEC) << "s" << endl;

    }
    r.print();
    cout << endl;
    beg = clock();
    r.del(11);
    end = clock() - beg;
    cout << "Delete Time: " << (double)end/((double)CLOCKS_PER_SEC) << "s" << endl;
    r.print();
    return 0;
}

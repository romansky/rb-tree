# Red-Black Tree
## Author: Billy Romansky
## COSC 320
## Dr. Anderson

I approached the problem in a very simple way by recreating the code taught in lecture to implement a Red-Black Tree data structure. The data structure relies on node structures that point to other child nodes.

The theoretical time complexity of every case of a Red-Black Tree is O(log(n)).

A problem I ran into while implementing this data structure was I forgot to create public wrapper functions for the insert, delete, and print functions of the tree. I believe implementing these late and after writing most of the code caused issues and inefficiencies.

I believe my late implementation of the wrapper functions made my tree contain key-less nodes during the different operations and given more time I would fix that.

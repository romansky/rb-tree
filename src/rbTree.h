#ifndef RBTREE_H
#define RBTREE_H

#include <iostream>
using namespace std;

enum color_t {RED,BLACK};

class rbTree {
    private:
        struct node {
            int key;
            color_t color;
            node* left;
            node* right;
            node* parent;
        };
        node* root;
        static node* const nil;
        void insertFix(node* n);
        void deleteFix(node* n ,node* m, color_t c);
        node* search(int k, node* n);
        void rotateL(node* m);
        void rotateR(node* m);
        void remove(node* n);
        node* min(node* n);
        node* max(node* n);
        node* successor(node* n);
        void transplant(node* x, node* y);
        void inOrder(node* n);
        void print(node* n);
    public:
        rbTree() {
            root = nil;
            root->parent = nil;
        }
        ~rbTree() {
            delete nil;
            rmTree(root);
        }
        void rmTree(node* n) {
            if(n == nil) {
                return;
            }
            rmTree(n->left);
            rmTree(n->right);
        }
        void insert(int k);
        void del(int k);
        void search(int k);
        node* min();
        node* max();
        node* successor();
        void inOrder();
        void print();

};

#include "rbTree.cpp"
#endif

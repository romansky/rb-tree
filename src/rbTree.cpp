#include "rbTree.h"

rbTree::node* const rbTree::nil = new node({0,BLACK,nullptr,nullptr,nullptr});

void rbTree::insert(int k)
{
    node* n = new node;
    n->key = k;
    n->parent = nil;
    if(root==nil) {
        root = n;
        n->parent = nil;
    } else {
        node* x = nil;
        node* y = root;
        while(y != nil)
        {
            x = y;
            if (k < y->key) {
                y = y->left;
            } else {
                y = y->right;
            }
        }
        if (k < x->key) {
            x->left = n;
        } else {
            x->right = n;
        }
        n->parent = y;
    }
    n->color = RED;
    n->left = nil;
    n->right = nil;
    insertFix(n);
}

void rbTree::del(int k)
{
    node* cursor = new node;
    cursor = search(k,root);
    if (cursor == nil) {
        cout << "DNE" << endl;
        return;
    } else {
        remove(cursor);
    }
}

void rbTree::remove(node* n)
{
    color_t clr;
    node* child;
    node* t;
    if (n->left == nil) {
        child = n->right;
        transplant(n,child);
    } else if (n->right == nil) {
        child = n->left;
        transplant(n,child);
    } else {
        t = min(n->right);
        clr = t->color;
        child = t->right;
        if (t->parent == n) {
            child->parent = n;
        } else {
            transplant(t, t->right);
            t->right = n->right;
            t->right->parent = t;
        }
        transplant(n,t);
        t->left = n->left;
        t->left->parent = t;
        t->color = n->color;
    }
    deleteFix(n,t,clr);
}

void rbTree::deleteFix(node* m, node* n, color_t c)
{
    if (c == BLACK) {
        bool side;
        node* sibling = new node;
        while((n != root)&&(n->color == BLACK))
        {
            if ((side = (n == n->parent->left))) {
                sibling = n->parent->right;
            } else {
                sibling = n->parent->left;
            }
            if (sibling->color == RED) {
                sibling->color = BLACK;
                n->parent->color = RED;
            }
        }
    }
}

void rbTree::transplant(node* x, node* y) {
    if (x->parent == nil) {
        root = y;
    } else if (x == x->parent->left) {
        x->parent->left = y;
    } else {
        x->parent->right = y;
    }
    y->parent = x->parent;
}

void rbTree::insertFix(node* n)
{
    node* a = nil;
    while(n->parent->color == RED)
    {
        if(n->parent == n->parent->left) {
            a = n->parent->parent->right;
            if (a->color == RED) {
                n->parent->color == BLACK;
                a->color = BLACK;
                n = n->parent->parent;
                n->color = RED;
            } else {
                if (n == n->parent->right) {
                    n = n->parent;
                    rotateL(n);
                }
                n->parent->color = BLACK;
                n->parent->parent->color = RED;
                rotateR(n);
            }
        } else {
            n = a->parent->parent->left;
            if (n->color == RED) {
                a->parent->color = BLACK;
                n->color = BLACK;
                a = a->parent->parent;
                a->color = RED;
            } else {
                if (a == a->parent->left) {
                    a = a->parent;
                    rotateR(a);
                }
                a->parent->color = BLACK;
                a->parent->parent->color = RED;
                rotateL(a);
            }
        }
    }
    root->color = BLACK;
}

void rbTree::rotateL(node* m)
{
    node* n;
    n = m->right;
    m->right = n->left;
    if (n->left->parent != nil) {
        n->left->parent = m;
    }
    n->parent = m->parent;
    if (m->parent == nil) {
        root = n;
    } else if (m == m->parent->left) {
        m->parent->left = n;
    } else {
        m->parent->right = n;
    }
    n->left = m;
    m->parent = n;
}

void rbTree::rotateR(node* m)
{
    node* n = m->left;
    m->left = n->left;
    if (n->right->parent != nil) {
        n->right->parent = m;
    }
    n->parent = m->parent;
    if (m->parent == nil) {
        root = n;
    } else if (m == m->parent->right) {
        m->parent->right = n;
    } else {
        m->parent->left = n;
    }
    n->right = m;
    m->parent = n;
}

rbTree::node* rbTree::search(int k, node* n)
{
    if ((n == nil)||(n->key == k)) {
        return n;
    } else if (k > n->key) {
        return search(k,n->right);
    } else {
        return search(k,n->left);
    }
}

rbTree::node* rbTree::min(node* n)
{
    if (n == nil) {
        n = new node;
        n->key = 0;
        n->left = nil;
        n->right = nil;
        n->parent = nil;
        n->color = BLACK;
    }
    while(n->left != nil)
    {
        n = n->left;
    }
    return n;
}

rbTree::node* rbTree::max(node* n)
{
    if (n == nil) { 
        n = new node;
        n->key = 0;
        n->left = nil;
        n->right = nil;
        n->parent = nil;
        n->color = BLACK;
    }
    while(n->right != nil)
    {
        n = n->right;
    }
    return n;
}

rbTree::node* rbTree::successor(node* n)
{
    if (n == nil)
    {
        n = new node;
        n->key = 0;
        n->left = nil;
        n->right = nil;
        n->parent = nil;
        n->color = BLACK;
    } else if (n->right != nil) {
        return min(n->right);
    } else if (n == this->max(root)) {
        return n;
    } else if (n = n->parent->right) {
        return n->parent;
    }
    return nullptr;
}

void rbTree::inOrder(node* n)
{
    if (n != nil) {
        inOrder(n->left);
        cout << n->key << " ";
        inOrder(n->right);
    }
}

void rbTree::print(node* n)
{
    if (n == nullptr) {
        return;
    }

    print(n->left);
    print(n->right);
    cout << "(" << n->color << "," << n->key << ")";
}

// Public wrapper function for print
void rbTree::print()
{
    print(root);
}
